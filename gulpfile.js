//遍历文件夹压缩所有图片
const gulp = require('gulp');

const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant'); //png图片压缩插件

const SearchDirectory = require("./SearchDirectory")

console.log("process.argv==", process.argv);
let basePath = "../../build/jsb-link/res/raw-assets/"
console.log("basePath==", basePath);

let searchPath = SearchDirectory.getAllDirectory(basePath);
// let searchPath = [];

// console.log("searchPath==", searchPath.length);

let srcPath = [];
for (let index = 0; index < searchPath.length; index++) {
    srcPath[index] = searchPath[index] +"/*.{png,jpg}";
    console.log("task==", searchPath[index]);
    gulp.task(searchPath[index], ()=>{
        return gulp.src(srcPath[index]).pipe(imagemin({
            progressive: true,
            use: [pngquant()] //使用pngquant来压缩png图片
        })).pipe(gulp.dest(searchPath[index]));
    });
}

// console.log("searchPath22==", searchPath);

gulp.task('default', searchPath, function () {
    console.log("default");
});



