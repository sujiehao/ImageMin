//遍历文件夹压缩所有图片
'use strict'

const fs = require('fs');
const path = require("path");

let minPathList = [];

function searchDirectory(searchPath){
    let files = fs.readdirSync(searchPath);
    for (let index = 0; index < files.length; index++) {
        const filename = files[index];
        let filedir = path.join(searchPath, filename);
        //获取当前文件的绝对路径  
        let stats = fs.statSync(filedir);
        if (stats.isDirectory()) {
            minPathList.push(filedir);
            searchDirectory(filedir);//递归，如果是文件夹，就继续遍历该文件夹下面的文件  
        }
    }
}

function getAllDirectory(searthPath){
    // minPathList = ["test"];
    minPathList = [];
    searchDirectory(searthPath);
    return minPathList;
}
// console.log("minPathList===", minPathList);

exports.getAllDirectory = getAllDirectory;



